package kg.attractor.lesson54.controller;

import kg.attractor.lesson54.model.Event;
import kg.attractor.lesson54.service.EventService;
import kg.attractor.lesson54.service.SubscribeOnEventService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/events")
public class EventController {
    EventService es;
    SubscribeOnEventService ss;

    public EventController(EventService es, SubscribeOnEventService ss) {
        this.es = es;
        this.ss = ss;
    }

    @GetMapping
    public Iterable<Event> getAllEvents() {
        return es.getAllEvents();

    }

    @GetMapping("/{event_id}/{email}")
    public String subscribeOnEvent(@PathVariable String event_id,
                                   @PathVariable String email) {
        String sub = ss.subscribe(event_id, email);
        return sub;
    }

}
