package kg.attractor.lesson54.controller;

import kg.attractor.lesson54.model.SubscribeOnEvent;
import kg.attractor.lesson54.service.EventService;
import kg.attractor.lesson54.service.SubscribeOnEventService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class SubscribeOnEventController {

    EventService es;
    SubscribeOnEventService ss;

    public SubscribeOnEventController(EventService es, SubscribeOnEventService ss) {
        this.es = es;
        this.ss = ss;
    }

    @GetMapping
    public Iterable<SubscribeOnEvent> getAllUsers() {
        return ss.getAllUsers();
    }

    @GetMapping("/{email}")
    public SubscribeOnEvent getUserByEmail(@PathVariable String email) {
        return ss.getByEmail(email);
    }

    @GetMapping("/delete/{sub_id}/{email}")
    public SubscribeOnEvent deleteSubscription(@PathVariable String sub_id,
                                               @PathVariable String email) {
        return ss.deleteSub(sub_id, email);
    }

}
