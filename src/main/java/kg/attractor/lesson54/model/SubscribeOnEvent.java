package kg.attractor.lesson54.model;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Document
@Data
public class SubscribeOnEvent {

    private String id = UUID.randomUUID().toString();
    private String email;
    private LocalDateTime dateTime;

    @DBRef
    private List<Event> events = new ArrayList<>();
}
