package kg.attractor.lesson54.model;

import kg.attractor.lesson54.util.Generator;
import lombok.*;
import org.springframework.data.mongodb.core.DbCallback;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.UUID;

@Document
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class Event {

    private String id = UUID.randomUUID().toString();
    private LocalDateTime date;
    private String title;
    private String description;


    public static Event random() {
        return builder()
                .date(LocalDateTime.now())
                .title(Generator.makeName())
                .description(Generator.makeDescription())
                .build();
    }
}
