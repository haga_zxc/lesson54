package kg.attractor.lesson54.repository;

import kg.attractor.lesson54.model.Event;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface EventRepository extends CrudRepository<Event, String> {
    Optional<Event> findById(String Id);
}
