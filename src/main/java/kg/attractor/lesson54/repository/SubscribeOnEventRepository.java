package kg.attractor.lesson54.repository;

import kg.attractor.lesson54.model.SubscribeOnEvent;
import org.springframework.data.repository.CrudRepository;

public interface SubscribeOnEventRepository extends CrudRepository<SubscribeOnEvent, String> {
    SubscribeOnEvent findByEmail(String email);
}
