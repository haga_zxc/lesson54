package kg.attractor.lesson54.util;

import kg.attractor.lesson54.model.Event;
import kg.attractor.lesson54.model.SubscribeOnEvent;
import kg.attractor.lesson54.repository.EventRepository;
import kg.attractor.lesson54.repository.SubscribeOnEventRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@Configuration
public class InitDatabase {
    @Bean
    CommandLineRunner init(EventRepository er, SubscribeOnEventRepository sr) {
        return (args) -> {
            er.deleteAll();
            sr.deleteAll();

            var demoUser = new SubscribeOnEvent();
            demoUser.setEmail("123@mail.ru");
            demoUser.setDateTime(LocalDateTime.now(ZoneId.of("Europe/Paris")));
            sr.save(demoUser);

            List<Event> events = Stream.generate(Event::random)
                    .limit(10)
                    .collect(toList());
            er.saveAll(events);
        };
    }
}
