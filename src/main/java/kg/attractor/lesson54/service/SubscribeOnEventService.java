package kg.attractor.lesson54.service;

import com.sun.source.tree.IdentifierTree;
import kg.attractor.lesson54.model.Event;
import kg.attractor.lesson54.model.SubscribeOnEvent;
import kg.attractor.lesson54.repository.EventRepository;
import kg.attractor.lesson54.repository.SubscribeOnEventRepository;
import org.springframework.stereotype.Service;

@Service
public class SubscribeOnEventService {
    private final SubscribeOnEventRepository sr;
    private final EventRepository er;

    public SubscribeOnEventService(SubscribeOnEventRepository sr, EventRepository er) {
        this.sr = sr;
        this.er = er;
    }

    public String subscribe(String id, String email) {
        var event = er.findById(id).get();
        var subscribe = sr.findByEmail(email);
        String result = "";
        int count = 0;
        if (subscribe.getDateTime().isBefore(event.getDate())) {
            for (Event e : subscribe.getEvents()) {
                if (e.getId().equals(event.getId())) {
                    count++;
                    result = "You already subscribed for this event";
                    System.out.println(result);
                    return result;
                }
            }
            if (count == 0) {
                result = "You successfully subscribed for this event";
                subscribe.getEvents().add(event);
                sr.save(subscribe);
                return result;
            }
        } else {
            result = "This event has closed";
            return result;
        }
        return result;
    }

    public Iterable<SubscribeOnEvent> getAllUsers() {
        var users = sr.findAll();
        return users;
    }

    public SubscribeOnEvent getByEmail(String email) {
        var user = sr.findByEmail(email);
        return user;
    }

    public SubscribeOnEvent deleteSub(String id, String email) {
        var sub = sr.findByEmail(email);
        sub.getEvents().removeIf(s -> s.getId().equals(id));
        sr.save(sub);
        return sub;
    }
}
