package kg.attractor.lesson54.service;

import kg.attractor.lesson54.model.*;
import kg.attractor.lesson54.repository.*;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EventService {
    private final SubscribeOnEventRepository sr;
    private final EventRepository er;

    public EventService(SubscribeOnEventRepository sr, EventRepository er) {
        this.sr = sr;
        this.er = er;
    }

    public Iterable<Event> getAllEvents() {
        return er.findAll();
    }

    public Optional<Event> getEventById(String id) {
        return er.findById(id);
    }

}
